object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 261
  ClientWidth = 262
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 31
    Height = 61
    Caption = 'Nome'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Cambria Math'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 57
    Height = 61
    Caption = 'Treinador'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Cambria Math'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 96
    Width = 29
    Height = 61
    Caption = 'Nivel'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Cambria Math'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBEdit1: TDBEdit
    Left = 93
    Top = 28
    Width = 121
    Height = 21
    DataField = 'Nome'
    DataSource = DataModule2.DataSource1
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 93
    Top = 116
    Width = 121
    Height = 21
    DataField = 'Nivel'
    DataSource = DataModule2.DataSource1
    TabOrder = 1
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 93
    Top = 72
    Width = 145
    Height = 21
    DataField = 'IdTreinador'
    DataSource = DataModule2.DataSource1
    KeyField = 'id'
    ListField = 'id'
    ListSource = DataModule2.DataSource2
    TabOrder = 2
  end
  object Button1: TButton
    Left = 40
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
  end
  object Button2: TButton
    Left = 152
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 4
    OnClick = Button2Click
  end
end
