unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit2, Unit3, Vcl.StdCtrls, Vcl.DBCtrls,
  Vcl.ExtCtrls, Vcl.Imaging.pngimage;

type
  TForm1 = class(TForm)
    DBLookupListBox1: TDBLookupListBox;
    BtnEditar: TButton;
    BtnInserir: TButton;
    BtnDeletar: TButton;
    LbId: TLabel;
    LbTreinador: TLabel;
    LbNome: TLabel;
    LbNivel: TLabel;
    DbTextID: TDBText;
    DBTextIdTreinador: TDBText;
    DBTextNomePokemon: TDBText;
    DBTextNivel: TDBText;
    Image1: TImage;
    procedure BtnEditarClick(Sender: TObject);
    procedure BtnDeletarClick(Sender: TObject);
    procedure BtnInserirClick(Sender: TObject);
    procedure DBLookupListBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm1.BtnDeletarClick(Sender: TObject);
begin
DataModule2.FDQuery1.Delete();
end;

procedure TForm1.BtnEditarClick(Sender: TObject);
begin
Form3 := TForm3.Create(Application);
Form3.Visible := True;
end;

procedure TForm1.BtnInserirClick(Sender: TObject);
begin
DataModule2.FDQuery1.Append();
Form3 := TForm3.Create(Application);
Form3.Visible := True;
end;


procedure TForm1.DBLookupListBox1Click(Sender: TObject);
var url: string;
begin
 url:= DataModule2.FDQuery1.FieldByName('url').AsString;
 Image1.Picture.LoadFromFile('imagens/' +url);
end;

end.
