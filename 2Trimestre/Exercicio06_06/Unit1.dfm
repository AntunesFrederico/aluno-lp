object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 410
  ClientWidth = 678
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 64
    Top = 48
    Width = 185
    Height = 241
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Memo2: TMemo
    Left = 384
    Top = 48
    Width = 185
    Height = 241
    Lines.Strings = (
      'Memo2')
    TabOrder = 1
  end
  object BtnLenght: TButton
    Left = 280
    Top = 46
    Width = 75
    Height = 25
    Caption = 'Lenght'
    TabOrder = 2
    OnClick = BtnLenghtClick
  end
  object BtnContains: TButton
    Left = 280
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Contains'
    TabOrder = 3
    OnClick = BtnContainsClick
  end
  object BtnTrim: TButton
    Left = 280
    Top = 134
    Width = 75
    Height = 25
    Caption = 'Trim'
    TabOrder = 4
    OnClick = BtnTrimClick
  end
  object BtnLowerCase: TButton
    Left = 280
    Top = 176
    Width = 75
    Height = 25
    Caption = 'LowerCase'
    TabOrder = 5
    OnClick = BtnLowerCaseClick
  end
  object BtnUpperCase: TButton
    Left = 280
    Top = 222
    Width = 75
    Height = 25
    Caption = 'UpperCase'
    TabOrder = 6
    OnClick = BtnUpperCaseClick
  end
  object BtnReplace: TButton
    Left = 280
    Top = 264
    Width = 75
    Height = 25
    Caption = 'Replace'
    TabOrder = 7
    OnClick = BtnReplaceClick
  end
end
