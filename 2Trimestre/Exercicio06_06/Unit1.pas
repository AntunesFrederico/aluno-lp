unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Memo2: TMemo;
    BtnLenght: TButton;
    BtnContains: TButton;
    BtnTrim: TButton;
    BtnLowerCase: TButton;
    BtnUpperCase: TButton;
    BtnReplace: TButton;
    procedure BtnLenghtClick(Sender: TObject);
    procedure BtnTrimClick(Sender: TObject);
    procedure BtnLowerCaseClick(Sender: TObject);
    procedure BtnUpperCaseClick(Sender: TObject);
    procedure BtnReplaceClick(Sender: TObject);
    procedure BtnContainsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BtnContainsClick(Sender: TObject);
var c: string;
begin
  c:= Memo1.Text;
  Memo2.Text:= c.Contains('a').ToString;
end;

procedure TForm1.BtnLenghtClick(Sender: TObject);
var tamanho:integer;
begin
  tamanho:= Length(Memo1.Text);
  Memo2.Text:= inttostr(tamanho);
end;


procedure TForm1.BtnLowerCaseClick(Sender: TObject);
var minusculo: string;
begin
  minusculo:= LowerCase(Memo1.Text);
  Memo2.Text:= minusculo;
end;

procedure TForm1.BtnReplaceClick(Sender: TObject);
var troquinha: string;
begin
  troquinha := Memo1.Text;
  Memo2.Text:= troquinha.Replace(' ' , 'TOP');
end;

procedure TForm1.BtnTrimClick(Sender: TObject);
var t: string;
begin
  t:= Trim(Memo1.Text);
  Memo2.Text:= t.Replace(' ', '#');
end;

procedure TForm1.BtnUpperCaseClick(Sender: TObject);
var maisculo: string;
begin
  maisculo:= UpperCase(Memo1.Text);
  Memo2.Text:= maisculo;
end;

end.
